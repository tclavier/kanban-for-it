REVEAL_JS_VERSION=3.8.0
REVEAL_JS_DIR=reveal.js-$(REVEAL_JS_VERSION)
FORK_AWESOME_VERSION=1.1.7
FORK_AWESOME_DIR=Fork-Awesome-$(FORK_AWESOME_VERSION)
TEXFILES = $(wildcard *.tex)
CLEAN=*~ *.rtf *.ps *.log *.dvi *.aux *.out *.html *.bak *.toc *.pl *.4ct *.4tc *.lg *.sxw *.tmp *.xref *.idv *.tns

all: public/index.html

reveal.js.zip:
	wget -q -O reveal.js.zip https://github.com/hakimel/reveal.js/archive/$(REVEAL_JS_VERSION).zip

fork-awesome.zip:
	wget -q -O fork-awesome.zip https://github.com/ForkAwesome/Fork-Awesome/archive/$(FORK_AWESOME_VERSION).zip

public/$(REVEAL_JS_DIR): reveal.js.zip
	unzip -o reveal.js.zip -d public

public/$(FORK_AWESOME_DIR): fork-awesome.zip
	unzip -o fork-awesome.zip -d public

public/index.html: slides.md public/$(REVEAL_JS_DIR) public/$(FORK_AWESOME_DIR)
	mkdir -p public
	cp -r includes public/
	cp azae.css public/
	pandoc -t revealjs --slide-level=2 -s -o public/index.html slides.md -V revealjs-url=./$(REVEAL_JS_DIR) -V theme=white --css=azae.css

%.pdf: %.tex
	echo "make " $<
	pdflatex  $<
	pdflatex  $<

clean:
	rm -rf $(CLEAN) public

pdf: $(patsubst %.tex,%.pdf,$(TEXFILES))

ci:
	inotify-hookable -f slides.md -f azae.css -w includes  -c "make"
